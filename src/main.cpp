#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
#include "SoftwareSerial.h"
#include "MHZ19.h"
#include "PMS.h"
#include "Adafruit_BME280.h"

void drawProgressBar();
void getBME280Reading();
void getCO2Reading();
void getPMReading();

// Display
SSD1306Wire display(0x3c, 5, 4);
int counter = 0;

// BME280
// SCL -> SCL (pin 22)
// SDA -> SDA (pin 21)
#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C
unsigned long delayTime;

// MH-z19B
SoftwareSerial ss(18, 23); // RX, TX
MHZ19 mhz(&ss);

// PMS5003
PMS pms(Serial1);
PMS::DATA data;

// Button
const int buttonPin = 0;
int state, previousState;

void setup()
{
    Serial.begin(9600);

    // Display setup
    display.init();
    // display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    
    // BME280 setup
    bool status;    
    status = bme.begin(0x76);
    if (!status) 
    {
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
    }

    // MH-Z19B CO2 sensor setup
    ss.begin(9600);

    // PMS5003 setup
    pms.passiveMode();
    pms.sleep();
}

void loop()
{
    // Check for button press
    state = digitalRead(buttonPin);  
    if (state != previousState && state == LOW) {
        // Do thing
        getBME280Reading();
    }
    previousState = state;
}

void drawProgressBar() 
{
  int progress = (counter / 5) % 100;
  // draw the progress bar
  display.drawProgressBar(0, 32, 120, 10, progress);

  // draw the percentage as String
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 15, String(progress) + "%");
}

void getBME280Reading() 
{
    // Only needed in forced mode! In normal mode, you can remove the next line.
    bme.takeForcedMeasurement(); // has no effect in normal mode

    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();
}

void getCO2Reading() 
{
    MHZ19_RESULT response = mhz.retrieveData();
    if (response == MHZ19_RESULT_OK)
    {
        Serial.print(F("CO2: "));
        Serial.println(mhz.getCO2());
        Serial.print(F("Min CO2: "));
        Serial.println(mhz.getMinCO2());
        Serial.print(F("Temperature: "));
        Serial.println(mhz.getTemperature());
        Serial.print(F("Accuracy: "));
        Serial.println(mhz.getAccuracy());
    }
    else
    {
        Serial.print(F("Error, code: "));
        Serial.println(response);
    }
}

void getPMReading()
{
    pms.wakeUp();
    delay(30000); // wait 30 sec before reading

    Serial1.println("Send read request...");
    pms.requestRead();

    Serial1.println("Wait max. 1 second for read...");
    if (pms.readUntil(data))
    {
        Serial1.print("PM 1.0 (ug/m3): ");
        Serial1.println(data.PM_AE_UG_1_0);

        Serial1.print("PM 2.5 (ug/m3): ");
        Serial1.println(data.PM_AE_UG_2_5);

        Serial1.print("PM 10.0 (ug/m3): ");
        Serial1.println(data.PM_AE_UG_10_0);
    }
    else
    {
        Serial1.println("No data.");
    }

    pms.sleep();
}